import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  const button = document.querySelector(".button");
  button.addEventListener("click", () => {
    alert("💣");
  });
  let article = document.createElement('article');
  article.classList.add('message');
  article.textContent = 'Plain message.';

  const bodyEl = document.getElementById('body');
  bodyEl.addEventListener("click", () => {
    for(let i = 0; i < 5; i++){
      bodyEl.prepend(article.cloneNode(1));
    }
  });

});
